#ifndef InvokeAction_h
#define InvokeAction_h

#include "Action.h"
typedef void (*InvokeFunc)(void);

class InvokeAction : public Action
{
    public:
        InvokeAction(InvokeFunc fn);
        ~InvokeAction();
        void update(unsigned long deltaTime);
    protected:
        InvokeFunc _fn = 0;
};

#endif