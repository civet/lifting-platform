#include "DelayAction.h"

DelayAction::DelayAction(long duration)
{
    _duration = duration;
    _remainedTime = _duration;
}

DelayAction::~DelayAction()
{
    
}

void DelayAction::update(unsigned long deltaTime)
{
    if(_isCompleted) return;

    _remainedTime -= deltaTime;
    if(_remainedTime <= 0) {
        complete();
    }
}

void DelayAction::reset()
{
    _isCompleted = false;
    _remainedTime = _duration;
}