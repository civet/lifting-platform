#ifndef Action_h
#define Action_h

class Action
{
    public:
        Action();
        virtual ~Action();
        virtual void update(unsigned long deltaTime);
        virtual void reset();
        void setBlocking(bool value);
        void complete();
        void onComplete(void (*handler)(Action*));        
        bool isCompleted();
        bool isBlocking();
    protected:
        bool _isBlocking = true; // default is blocking
        bool _isCompleted = false;
        void (*_completeHandler)(Action*) = 0;
};

#endif