#ifndef RepeatAction_h
#define RepeatAction_h

#include "Action.h"
typedef bool (*RepeatHandler)(unsigned long);

class RepeatAction : public Action
{
    public:
        RepeatAction(RepeatHandler handler);
        ~RepeatAction();
        void update(unsigned long deltaTime);
    protected:
        RepeatHandler _repeatHandler = 0;
};

#endif