#include "ActionList.h"

ActionList::ActionList()
{
    _list = new LinkedList<Action*>();
}

ActionList::~ActionList()
{
    //delete _list;
}

void ActionList::update(unsigned long deltaTime)
{
    if(_isCompleted) return;

    Action* action;
    bool completed, blocking;
    int numCompleted = 0;
    int numActions = _list->size();
    
    // update children
    for(int i = 0; i < numActions; i++) 
    {        
        action = _list->get(i);
        completed = action->isCompleted();
        blocking = action->isBlocking();

        if(!completed) {
            action->update(deltaTime);
        }

        if(completed) {
            numCompleted++;
            continue;
        }
        
        if(blocking) break;
    }

    // all done
    if(numCompleted == numActions) {
        complete();
    }
}

void ActionList::setChildrenBlocking(bool value)
{
    Action* action;
    int numActions = _list->size();
    for(int i = 0; i < numActions; i++) {
        action = _list->get(i);
        action->setBlocking(value);
    }
}

void ActionList::resetChildren()
{
    Action* action;
    int numActions = _list->size();
    for(int i = 0; i < numActions; i++) {
        action = _list->get(i);
        action->reset();
    }
}

void ActionList::reset()
{
    resetChildren();

    _isCompleted = false;
}

void ActionList::add(Action* action)
{   
    _list->add(action);
}

void ActionList::remove(int index)
{
    _list->remove(index);
}

void ActionList::clear(bool deep)
{
    if(deep) {
        while(_list->size() > 0) {
            delete _list->shift();
        }
    }
    else {
        _list->clear();
    }
}

void ActionList::addSerial(Action* actions[], int numActions)
{
    Action* action;
    for(int i = 0; i < numActions; i++) {
        action = actions[i];
        action->setBlocking(true);
        _list->add(action);
    }
}

void ActionList::addParallel(Action* actions[], int numActions)
{
    Action* action;
    for(int i = 0; i < numActions; i++) {
        action = actions[i];
        action->setBlocking(false);
        _list->add(action);
    }
}

ActionList* ActionList::createSerial(Action* actions[], int numActions)
{
    ActionList* actionList = new ActionList();
    Action* action;
    for(int i = 0; i < numActions; i++) {
        action = actions[i];
        action->setBlocking(true);
        actionList->add(action);
    }
    return actionList;
}

ActionList* ActionList::createParallel(Action* actions[], int numActions)
{
    ActionList* actionList = new ActionList();
    Action* action;
    for(int i = 0; i < numActions; i++) {
        action = actions[i];
        action->setBlocking(false);
        actionList->add(action);
    }
    return actionList;
}