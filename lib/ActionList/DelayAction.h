#ifndef DelayAction_h
#define DelayAction_h

#include "Action.h"

class DelayAction : public Action
{
    public:
        DelayAction(long duration);
        ~DelayAction();
        void update(unsigned long deltaTime);
        void reset();
    protected:
        long _duration;
        long _remainedTime;
};

#endif