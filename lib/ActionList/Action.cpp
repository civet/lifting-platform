#include "Action.h"

Action::Action()
{
    
}

Action::~Action()
{
    
}

void Action::update(unsigned long deltaTime)
{
    // do nothing
}

void Action::reset()
{
    _isCompleted = false;
}

void Action::setBlocking(bool value)
{
    _isBlocking = value;
}

void Action::onComplete(void (*handler)(Action*))
{
    _completeHandler = handler;
}

void Action::complete()
{   
    _isCompleted = true;
    if(_completeHandler) _completeHandler(this);
}

bool Action::isCompleted()
{   
    return _isCompleted;
}

bool Action::isBlocking()
{   
    return _isBlocking;
}