#ifndef ActionScheduler_h
#define ActionScheduler_h

#include <Arduino.h>
#include <LinkedList.h>
#include "Action.h"
typedef unsigned long (*ElapsedTimeFunc)(void);

class ActionScheduler
{
    public:
        ActionScheduler(ElapsedTimeFunc fn);
        ~ActionScheduler();
        void update(unsigned long deltaTime);
        ActionScheduler& add(Action* action);
        void remove(int index);
        void clear();
        void process();
    private:
        LinkedList<Action*> *_list;
        ElapsedTimeFunc _millis;
        unsigned long _previousTime, _currentTime;
        unsigned long _deltaTime;
};

#endif