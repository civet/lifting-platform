#ifndef ActionPreset_h
#define ActionPreset_h

#include "DelayAction.h"
#include "InvokeAction.h"
#include "RepeatAction.h"
#include "DelayIfAction.h"

typedef DelayAction Delay;
typedef InvokeAction Invoke;
typedef RepeatAction Repeat;
typedef DelayIfAction DelayIf;

#endif