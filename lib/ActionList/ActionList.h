#ifndef ActionList_h
#define ActionList_h

#include <LinkedList.h>
#include "Action.h"

class ActionList : public Action
{
    public:
        ActionList();
        ~ActionList();
        void update(unsigned long deltaTime);
        void reset();
        void setChildrenBlocking(bool value);
        void add(Action* action);
        void remove(int index);
        void clear(bool deep = false);
        void addSerial(Action* actions[], int numActions);
        void addParallel(Action* actions[], int numActions);
        static ActionList* createSerial(Action* actions[], int numActions);
        static ActionList* createParallel(Action* actions[], int numActions);
    protected:
        LinkedList<Action*> *_list;
        void resetChildren();
};

#endif