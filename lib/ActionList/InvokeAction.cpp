#include "InvokeAction.h"

InvokeAction::InvokeAction(InvokeFunc fn)
{
    _fn = fn;
}

InvokeAction::~InvokeAction()
{
    
}

void InvokeAction::update(unsigned long deltaTime)
{
    if(_isCompleted) return;
    
    if(_fn) _fn();
    
    complete();
}