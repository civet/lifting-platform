#include "DelayIfAction.h"

DelayIfAction::DelayIfAction(long duration, ConditionHandler handler)
{
    _duration = duration;
    _remainedTime = _duration;

    _conditionHandler = handler;
}

DelayIfAction::~DelayIfAction()
{
    
}

void DelayIfAction::update(unsigned long deltaTime)
{
    if(_isCompleted) return;

    if(!_passed) {
        _passed = _conditionHandler();
        
        if(!_passed) {
            complete();
            return;
        }
    }

    _remainedTime -= deltaTime;
    if(_remainedTime <= 0) {
        complete();
    }
}

void DelayIfAction::reset()
{
    _isCompleted = false;
    _remainedTime = _duration;
    _passed = false;
}