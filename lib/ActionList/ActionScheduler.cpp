#include "ActionScheduler.h"

ActionScheduler::ActionScheduler(ElapsedTimeFunc fn)
{
    _millis = fn;    
    _list = new LinkedList<Action*>();
    _previousTime = 0;
    _currentTime = 0;
    _deltaTime = 0;
}

ActionScheduler::~ActionScheduler()
{
    
}

ActionScheduler& ActionScheduler::add(Action* action)
{   
    _list->add(action);

    return *this;
}

void ActionScheduler::remove(int index)
{
    _list->remove(index);
}

void ActionScheduler::clear()
{
    _list->clear();
}

void ActionScheduler::process()
{
    _currentTime = millis();
    _deltaTime = _currentTime - _previousTime;
    _previousTime = _currentTime;

    Action* action;
    int numActions = _list->size();
    for(int i = 0; i < numActions; i++) {
        action = _list->get(i);
        action->update(_deltaTime);
    }    
}