#ifndef DelayIfAction_h
#define DelayIfAction_h

#include "Action.h"
typedef bool (*ConditionHandler)(void);

class DelayIfAction : public Action
{
    public:
        DelayIfAction(long duration, ConditionHandler handler);
        ~DelayIfAction();
        void update(unsigned long deltaTime);
        void reset();
    protected:
        long _duration;
        long _remainedTime;
        bool _passed = false;
        ConditionHandler _conditionHandler = 0;
};

#endif