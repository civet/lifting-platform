#include "RepeatAction.h"

RepeatAction::RepeatAction(RepeatHandler handler)
{
    _repeatHandler = handler;
}

RepeatAction::~RepeatAction()
{
    
}

void RepeatAction::update(unsigned long deltaTime)
{
    if(_isCompleted) return;

    bool ok = _repeatHandler(deltaTime);
    
    if(ok) complete();
}