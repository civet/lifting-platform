#ifndef PushButton_h
#define PushButton_h

#include "ReComponent.h"

class PushButton : public ReComponent
{
    public:
        PushButton(int pin);
        PushButton& begin();
        PushButton& setId(int id);
        PushButton& onPress(void (*callback)(int));
        PushButton& onRelease(void (*callback)(int));
        void update(unsigned long deltaTime);
    private:
        static const int DELAY_TIME = 50;
        int _id = 0;
        int _pin;
        long _remainedTime;
        int _state;
        int _lastState;
        void (*_pressHandler)(int) = 0;
        void (*_releaseHandler)(int) = 0;
};

#endif