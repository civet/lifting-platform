#ifndef ReComponentManager_h
#define ReComponentManager_h

#include <LinkedList.h>
#include "ReComponent.h"
typedef unsigned long (*MillisFunc)(void);

class ReComponentManager
{
    public:
        ReComponentManager(MillisFunc fn);
        ReComponentManager& add(ReComponent* component);
        ReComponentManager& remove(int index);
        ReComponentManager& clear();
        void process();
    private:
        LinkedList<ReComponent*> *_list;
        MillisFunc _millis;
        unsigned long _previousTime, _currentTime;
        unsigned long _deltaTime;
};

#endif