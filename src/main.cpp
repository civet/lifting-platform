#include <Arduino.h>
#include <A4988.h>
#include "ActionScheduler.h"
#include "ActionList.h"
#include "ActionPreset.h"
#include "ReComponentManager.h"
#include "PushButton.h"

// Stepper configuration
#define RPM 12
#define MICROSTEP 1
#define MOTOR_STEPS 1600
#define DIR_PIN 13
#define STEP_PIN 12
#define ENABLE_PIN 14

// the direction at start (-1 or 1)
#define STARTING_DIRECTION 1

// how many steps can move for each iteration
#define MOVING_DISTANCE 200 * 20

// delay when stay at endpoint (millisecond)
#define ENDPOINT_DELAY 1000 * 5

// LimitSwitch configuration
#define BUTTON_A_PIN 0
#define BUTTON_B_PIN 1
 

A4988 stepper(MOTOR_STEPS, DIR_PIN, STEP_PIN, ENABLE_PIN);
int stepperDirection = STARTING_DIRECTION;
int stepperMovingDistance = MOVING_DISTANCE;

PushButton buttonA(BUTTON_A_PIN);
PushButton buttonB(BUTTON_B_PIN);
int buttonAState = 0;
int buttonBState = 0;

bool startWaiting = false;

ActionScheduler actionScheduler(millis);
ActionList stepperActions;

ReComponentManager componentManager(millis);


void handleButtonPress(int id)
{
    switch (id) {
        case 0:
            buttonAState = 1;
            break;
        case 1: 
            buttonBState = 1;
            break;
        default: 
            break;
    }
}

void handleButtonRelease(int id)
{
    switch (id) {
        case 0:
            buttonAState = 0;
            break;
        case 1: 
            buttonBState = 0;
            break;
        default: 
            break;
    }
}

void setup() 
{
    Serial.begin(115200);

    // init stepper
    stepper.begin(RPM, MICROSTEP);
    stepper.setEnableActiveState(LOW);
    stepper.enable();

    // init limit switch
    buttonA.begin()
        .setId(0)
        .onPress(handleButtonPress)
        .onRelease(handleButtonRelease);

    buttonB.begin()
        .setId(1)
        .onPress(handleButtonPress)
        .onRelease(handleButtonRelease);

    componentManager
        .add(&buttonA)
        .add(&buttonB);

    actionScheduler
        .add(&stepperActions);

    // startup
    stepperActions.add(
        new Invoke([]() {
            stepper.startMove(stepperMovingDistance * stepperDirection);
        })
    );

    stepperActions.add(
        new Repeat([](unsigned long d) -> bool {
            unsigned long remainedTime = stepper.nextAction();

            // move forward until reach the limit
            int buttonState = (stepperDirection == STARTING_DIRECTION ? buttonAState : buttonBState);
            bool reachedLimit = (buttonState == 1);
            if (reachedLimit) {
                stepperDirection *= -1;
                startWaiting = true;
                return true;
            }

            // or no more step
            bool finish = remainedTime <= 0;
            if (finish) {
                return true;
            }

            return false;
        })
    );

    stepperActions.add(
        new DelayIf(ENDPOINT_DELAY, []() -> bool {
            bool flag = startWaiting;
            startWaiting = false;
            return flag;
        })
    );

    // as loopable actions
    stepperActions.onComplete([](Action* target) {
        target->reset();
    });

    // reset actions in order to start immediately
    stepperActions.reset();
}

void loop()
{
    componentManager.process();
    actionScheduler.process();
}